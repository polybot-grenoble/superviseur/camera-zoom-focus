#!/bin/python3

import numpy as np
import cv2 as cv
import glob
import cmdParser
import os
import json

def compute_chessboards(square_size_mm=1,chess_size=(9,6),fisheye=False) :
    print("Computing chessboards")
    if square_size_mm == 1:
        square_size_mm=31

    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    if fisheye:
        objp = np.zeros((1,chess_size[0]*chess_size[1],3), np.float32)
        objp[0,:,:2]=np.mgrid[0:chess_size[0],0:chess_size[1]].T.reshape(-1,2)
        objp[0,:,:2]=objp[0,:,:2]*square_size_mm
    else:
        objp = np.zeros((chess_size[0]*chess_size[1],3), np.float32)
        objp[:,:2]=np.mgrid[0:chess_size[0],0:chess_size[1]].T.reshape(-1,2)
        objp[:,:2]=objp[:,:2]*square_size_mm
    
    objpoints = []
    imgpoints = []

    images = glob.glob("./img/*")

    if len(images) == 0 :
        print("Error loading images.")
        return

    cv.namedWindow("img", cv.WINDOW_NORMAL)
    cv.resizeWindow("img",1280,720)

    for fname in images:
        img = cv.imread(fname)

        # Affiche le nom du fichier - Permet de supprimer les images qui font freeze le programme
        print(fname)
        
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        # Find the chess board corners
        ret, corners = cv.findChessboardCorners(gray, chess_size, cv.CALIB_CB_ADAPTIVE_THRESH+cv.CALIB_CB_FAST_CHECK+cv.CALIB_CB_NORMALIZE_IMAGE)
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
            corners2 = cv.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
            imgpoints.append(corners2)
            # Draw and display the corners
            cv.drawChessboardCorners(img, chess_size, corners2, ret)
            cv.imshow("img", img)
            cv.waitKey(500)
    cv.destroyAllWindows()

    if len(imgpoints) == 0:
        print("No chessboard detected.")
        return None, None, None
    
    return objpoints, imgpoints


def get_calibration_parameters(image_size_px,square_size_mm=1,output=""):
    
    objpoints, imgpoints = compute_chessboards(square_size_mm,(9,6),False)

    if objpoints == None or imgpoints == None:
        return

    # Calibrate
    ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, image_size_px, None, None)

    mean_error = 0
    for i in range(len(objpoints)):
        imgpoints2, _ = cv.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
        error = cv.norm(imgpoints[i], imgpoints2, cv.NORM_L2)/len(imgpoints2)
        mean_error += error
    
    print( "total error: {}".format(mean_error/len(objpoints)))

    write_camera_config(mtx,dist,image_size_px,False,output)




def get_fisheye_calibration_parameters(image_size_px,square_size_mm=1,output=""):

    calibration_flags = cv.fisheye.CALIB_RECOMPUTE_EXTRINSIC+cv.fisheye.CALIB_FIX_SKEW
    
    objpoints, imgpoints = compute_chessboards(square_size_mm,(9,6),True)

    if objpoints == None or imgpoints == None:
        return

    N_OK = len(objpoints)
    K = np.zeros((3, 3))
    D = np.zeros((4, 1))
    rvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(N_OK)]
    tvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(N_OK)]

    rms, _, _, _, _ = cv.fisheye.calibrate(objpoints,imgpoints,image_size_px,K,D,rvecs,tvecs,calibration_flags,(cv.TERM_CRITERIA_EPS+cv.TERM_CRITERIA_MAX_ITER, 30, 1e-6))

    mean_error = 0
    for i in range(len(objpoints)):
        imgpoints2, _ = cv.projectPoints(objpoints[i], rvecs[i], tvecs[i], K, D)
        error = cv.norm(imgpoints[i], imgpoints2, cv.NORM_L2)/len(imgpoints2)
        mean_error += error
    
    print( "total error: {}".format(mean_error/len(objpoints)))

    write_camera_config(K,D,image_size_px,True,output)




def write_camera_config(camera_matrix,dist_coeff,image_size_px,fisheye=False,output=""):
    if not camera_matrix.any() or not dist_coeff.any():
        print("Can't write empty matrices.")
        return

    if not output:
        print("Using default path.")
        output="config/default_config.json"

    try:
        from czf import czf
        controller = czf(1)
        data = {"focus": controller.get_focus(), "zoom": controller.get_zoom(), "resolution": [image_size_px[0],image_size_px[1]]}
    except:
        print("Writing default focus/zoom data.")
        data = {"focus": 1, "zoom": 0, "resolution": [image_size_px[0],image_size_px[1]]}
    
    f = open(output,"w")
    f.write(json.dumps(data))
    f.close()
        
    cv_file = cv.FileStorage(output, cv.FILE_STORAGE_APPEND)
    cv_file.write("camera", camera_matrix)
    cv_file.write("dist_coeffs", dist_coeff)
    cv_file.release()

    print("Config saved in {}".format(output))



def generate_image_set(image_size_px,size=10):
    from Camera import Camera

    cam = Camera(0,image_size_px)
    cam.start()

    print("Generating image set (size={})".format(size))
    print("---------------------------------")
    print("Change the chessboard orientation between each picture.")
    print("Press 'q' to exit.")
    print("WARNING : It overwrites the previous set.")
    print("---------------------------------")
    
    i=0
    exit=False

    if not os.path.exists("./img"):
        os.mkdir("./img")

    cv.namedWindow("Preview", 0)
        
    while i<size:
        print("Press 's' to save image. ({}/{})".format(i+1,size))    
        
        while True :
            img = cam.read()
            imgtemp = cv.resize(img, (1280, 720))
            cv.imshow("Preview", imgtemp)
            
            key=cv.waitKey(1)
            if key == ord("s"):
                break
            if key == ord("q"):
                exit=True
                break
        
        if exit:
            break

        cv.imwrite("./img/img{}.jpg".format(i), cam.read())
        i+=1

    cv.destroyAllWindows()
    cam.stop()

    if exit:
        print("Aborted.")
    else:
        print("Done.")



if __name__=="__main__":
    param = cmdParser.parse_cc_arguments()

    if param == None :
        exit()

    if param["gen"] != 0:
        generate_image_set(param["gen"],param["res"])

    if param["fisheye"]:
        get_fisheye_calibration_parameters((param["width"], param["height"]),param["size"],param["path"])
    else:
        get_calibration_parameters((param["width"], param["height"]),param["size"],param["path"])
