# camera-zoom-focus

## Introduction

Ce répertoire contient la librairie de contrôle de la caméra utilisée par le superviseur, dans le cadre de la Coupe de France de Robotique 2023.

Le programme est construit sur la base de la bibliothèque OpenCV en langage Python.
Pour compiler OpenCV, il faut se référer au [répertoire correspondant](https://gitlab.com/VulcanixFR/projet-opencv) de Julien Pierson. Si vous ne souhaitez pas compiler OpenCV (c'est long, c'est chiant), vous pouvez installer `python3-opencv` avec la commande ci-dessous.

```
sudo apt-get install python3-opencv
```

Ce programme a été construit autour de ce modèle de [caméra](https://www.arducam.com/docs/cameras-for-raspberry-pi/native-raspberry-pi-cameras/8mp-imx219-ptz-camera-module/). 

### Remerciements
README.md grandement inspiré des conseils du grand Pierson.

## Connexion de la caméra
TBD

## Installation
Si vous voulez accéder à la librairie depuis un projet situé dans un autre dossier, il faut ajouter le chemin de ce dossier dans les variables d'environnements.

```bash
export PYTHONPATH=’path/to/camera-zoom-focus’
```
___NB___: Il faut ajouter la variable dans le fichier `.bashrc` pour que celle-ci persiste.

## Exemples d'utilisation

### Test des différents fichiers

Chaque fichier peut être executé indépendament des uns et des autres afin de vérifier leur bon fonctionnement.

### Exemple : Modifier le zoom de l'objectif

```python
import czf
import time

controller=czf(0,1) # Camera 0, Bus I2C 1, No debug

controller.reset_zoom()
time.sleep(1)
controller.set_zoom(4000)
```

### Exemple : Régler automatiquement le focus

```python
import czf

controller=czf(0,1,True) # Camera 0, Bus I2C 1, Debug

controller.autofocus(100,2000,10) # Autofocus sur un intervalle de 100 à 2000 avec un pas de 10
```