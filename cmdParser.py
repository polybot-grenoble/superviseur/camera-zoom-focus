#!/bin/python3

import sys

def print_czf_help():
    print("Usage : python3 ./czf.py [OPTION]...")
    print("Options :")
    print("     -z <value>      Assigns zoom value to the camera (0,20000).")
    print("     -f <value>      Assigns focus value to the camera (0,20000).")
    print("     -a              Applies the sharpest focus to the camera. Overrides \"-f\" option.")
    print("     -i              Toggles IRCut.")
    print("     -r              Resets focus to 0.")
    print("     -x              Resets zoom to 0.")
    print("     -d              Runs in debug mode.")

def parse_czf_arguments():
    arg = sys.argv
    
    if len(arg)==1:
        print_czf_help()
        return None
    
    param = {"zoom": -1, "focus": -1, "autofocus": False, "debug": False, "ir": False, "reset": False, "resetZoom": True}
    
    i=1
    valid=True
    while valid and i<len(arg):
        if arg[i]=='-z':
            #Check value range
            param["zoom"]=int(arg[i+1])
            i+=2
        elif arg[i]=="-f":
            #Check value range
            param["focus"]=int(arg[i+1])
            i+=2
        elif arg[i]=="-a":
            param["autofocus"]=True
            i+=1
        elif arg[i]=="-d":
            param["debug"]=True
            i+=1
        elif arg[i]=="-i":
            param["ir"]=True
            i+=1
        elif arg[i]=="-r":
            param["reset"]=True
            i+=1
        elif arg[i]=="-x":
            param["resetZoom"]=True
            i+=1
        else:
            valid=False
            print("Invalid parameter : ", arg[i])
            print_czf_help()
    
    if valid:
        return param
    else:
        return None

def print_cc_help():
    print("Usage : python3 ./cc.py [OPTION]...")
    print("Options :")
    print("     --help         Show this.")
    print("     -g <value>     Generates a set of n images using the camera.")
    print("     -s <value>     Chessboard square size. (in mm)")
    print("     -f             Use fisheye camera.")
    print("     -w <value>     Specify width. (in px)")
    print("     -h <value>     Specify height. (in px)")
    print("     -o             Specify output path.")

def parse_cc_arguments():
    arg = sys.argv
    param = {"gen": 0, "fisheye": False, "size": 1, "height": 0, "width": 0, "path": ""}
    
    i=1
    valid=True
    while valid and i<len(arg):
        if arg[i]=="--help":
            i+=1
            valid=False
            print_cc_help()
        elif arg[i]=="-g":
            if arg[i+1]!=None:
                param["gen"]=int(arg[i+1])
            else:
                valid=False
                print_cc_help()
            i+=2
        elif arg[i]=="-s":
            if arg[i+1]!=None:
                param["size"]=float(arg[i+1])
            else:
                valid=False
                print_cc_help()
            i+=2
        elif arg[i]=="-f":
            param["fisheye"]=True
            i+=1
        elif arg[i]=="-w":
            if arg[i+1]!=None and arg[i+1][0]!="-":
                param["width"]=int(arg[i+1])
            else:
                valid=False
                print_cc_help()
            i+=2
        elif arg[i]=="-h":
            if arg[i+1]!=None and arg[i+1][0]!="-":
                param["height"]=int(arg[i+1])
            else:
                valid=False
                print_cc_help()
            i+=2
        elif arg[i]=="-o":
            if arg[i+1]!=None and arg[i+1][0]!="-":
                param["path"]=arg[i+1]
            else:
                valid=False
                print_cc_help()

            i+=2
        else:
            valid=False
            print("Invalid parameter : ", arg[i])
            print_cc_help()


    valid = param["res"] != None
    
    if valid:
        return param
    else:
        return None

if __name__ == "__main__":
    #print(parse_czf_arguments())
    print(parse_cc_arguments())