#!/bin/python3

import matplotlib.pyplot as plt
import time

from Focuser import Focuser
import cmdParser

class czf :
    focuser=None
    debug=None

    def __init__(self, camera_id: int=0, i2c_bus: int=1, debug: bool=False):
        """ Initiate the camera controller with the specified camera port and i2c bus.

        Parameters
        --------------
        camera_id : int
            Id of the camera.
        i2c_bus : int
            Id of the I2C bus to use.
        debug: bool
            Whether or not debug info should be printed. Use more memory if enabled.
        
        Returns
        --------------
        Nothing.
        """
        self.focuser    = Focuser(i2c_bus)
        self.debug      = debug
        self.camera_id  = camera_id

        if self.debug :
            print("------------------------------------")
            print("Running DEBUG mode. More memory will be used.")

    # FOCUS ---------------------------------------    
    def set_focus(self,focus: int):
        """ Change the focus of the camera.

        Parameters
        --------------
        focus : int
            Focus value to apply to the lens. Value should be between [0, 20000].
        
        Returns
        --------------
        Nothing.
        """
        self.focuser.set(Focuser.OPT_FOCUS, focus)
        print(focus)
    

    def get_focus(self) -> int:
        """ Get the focus value of the lens.

        Parameters
        --------------
        Nothing.
        
        Returns
        --------------
        int
            Focus value applied to the lens.
        """

        focus = self.focuser.get(Focuser.OPT_FOCUS)
        print(focus)
        return focus
    

    def reset_focus(self):
        """ Reset the focus to its default value (0).

        Parameters
        --------------
        Nothing.

        Returns
        --------------
        Nothing.
        """
        
        self.focuser.reset(Focuser.OPT_FOCUS)


    def autofocus(self, min_step: int, max_step: int, step: int=20):
        """ Set the focus to make the image as sharp as possible.

        The focus travels across its entire value range. The function computes the laplacian of the image for each focus value.
        Finally, it applies the focus linked to the highest laplacian value.

        Parameters
        --------------
        min_step : int
            Minimum value to apply focus on.
        max_step : int
            Maximum value to apply focus on.
        step: int
            Step per iterations. Lower is slower but more precise.

        Returns
        --------------
        Nothing.
        """

        # Check for default settings
        if not min_step :
            min_step = self.focuser.opts["OPT_FOCUS"]["MIN_VALUE"]

        if not max_step :
            max_step = self.focuser.opts["OPT_FOCUS"]["MAX_VALUE"]

        # Constraint check
        if min_step + step >= max_step:
            print("ERROR : min_step + step can't be higher than max_step.")
            exit(0)

        #Import and setup camera
        from Camera import Camera
        from Camera import get_laplacian

        cam=Camera(self.camera_id)
        cam.start()

        #Reset focus to avoid focus "overflow" behaviour.
        self.reset_focus()

        if(self.debug):
            t0=time.time()
            steps = [0]*((max_step-min_step)//step)
            lps = [0]*((max_step-min_step)//step)
            print("------------------------------------")
            print("min step: ", min_step)
            print("max step: ", max_step)
            print("step: ", step)
        
        #Initial value
        self.set_focus(min_step)
        laplacian = get_laplacian(cam.read()).var()
        optimized_focus = {"step": min_step, "laplacian": laplacian}
        i=1

        if(self.debug):
            steps[0]=min_step
            lps[0]=laplacian

        # Swipe across the whole range
        while (min_step + i*step) < max_step:
            self.set_focus(min_step + i*step)
            laplacian = get_laplacian(cam.read()).var()

            if laplacian>optimized_focus["laplacian"]:
                optimized_focus["step"] = min_step + i*step
                optimized_focus["laplacian"] = laplacian   
            
            if(self.debug):
                steps[i]=min_step + i*step
                lps[i]=laplacian

            i+=1

        cam.stop()
        self.set_focus(optimized_focus["step"])

        if self.debug:
            print("------------------------------------")
            print("autofocus exec time (s): ", time.time()-t0)
            print("------------------------------------")
            print("Applied focus")
            print("step: ", optimized_focus["step"])
            print("laplacian: ", optimized_focus["laplacian"])

            print(steps)
            print(lps)
            
            plt.plot(steps,lps)
            plt.ylabel('Laplacian')
            plt.xlabel('Steps')
            plt.show()



    # ZOOM ---------------------------------------    
    def set_zoom(self,zoom: int):
        """ Change the zoom of the camera.

        Parameters
        --------------
        zoom : int
            Zoom value to apply to the lens. Value should be between [0, 13000].
        
        Returns
        --------------
        Nothing.
        """
        
        self.focuser.set(Focuser.OPT_ZOOM, zoom)

        if self.debug :
            print("------------------------------------")
            print("Applied zoom : ", zoom)
    
    def get_zoom(self) -> int:
        """ Get the zoom value of the lens.

        Parameters
        --------------
        Nothing.
        
        Returns
        --------------
        int
            Zoom value applied to the lens.
        """
        if self.debug :
            zoom = self.focuser.get(Focuser.OPT_ZOOM)
            print("------------------------------------")
            print("Retrieved zoom : ", zoom)
            return zoom
        else:
            return self.focuser.get(Focuser.OPT_ZOOM)

    def reset_zoom(self):
        """ Reset the zoom to its default value (0).

        Parameters
        --------------
        Nothing.

        Returns
        --------------
        Nothing.
        """
        
        self.focuser.reset(Focuser.OPT_ZOOM)
                
        if self.debug :
            print("------------------------------------")
            print("Zoom value reset.")

    # IRCUT ---------------------------------------    
    def is_ircut(self) -> bool:
        """ Return the ircut state. Ircut mode allows the camera to "see" in the near infrared spectrum.

        Parameters
        --------------
        Nothing.

        Returns
        --------------
        bool
            Ircut state.
        """
        if self.debug :
            ircut=self.focuser.get(Focuser.OPT_IRCUT)
            print("------------------------------------")
            print("Retrieved ircut : ", ircut)
            return ircut == 1
        else:
            return self.focuser.get(Focuser.OPT_IRCUT) == 1

    def enable_ircut(self):
        """ Enable ircut mode. Ircut mode allows the camera to "see" in the near infrared spectrum.

        Parameters
        --------------
        Nothing.
        
        Returns
        --------------
        int
            Zoom value applied to the lens.
        """
        
        if not self.is_ircut():
            self.focuser.set(Focuser.OPT_IRCUT, 1)
            if self.debug :
                print("------------------------------------")
                print("Ircut enabled.")
    
    def disable_ircut(self):
        """ Disable ircut mode. Ircut mode allows the camera to "see" in the near infrared spectrum.

        Parameters
        --------------
        Nothing.
        
        Returns
        --------------
        int
            Zoom value applied to the lens.
        """
        if self.is_ircut():
            self.focuser.set(Focuser.OPT_IRCUT, 0)
            if self.debug :
                print("------------------------------------")
                print("Ircut disabled.")

if __name__ == "__main__":
    param = cmdParser.parse_czf_arguments()

    if param == None : exit()

    controller = czf(0,1,param["debug"])

    if param["ir"] :
        if controller.is_ircut():
            controller.disable_ircut()
        else:
            controller.enable_ircut()

    if param["reset"] :
        controller.reset_focus()
    
    if param["resetZoom"]:
        controller.reset_zoom()
    
    if param["autofocus"] :
        controller.autofocus(1900,2100,5)
        exit()


    if param["zoom"] >= 0 :
        controller.set_zoom(param["zoom"])
    else:
        controller.get_zoom()
    
    if param["focus"] >= 0 :
        controller.set_focus(param["focus"])
    else:
        controller.get_focus()