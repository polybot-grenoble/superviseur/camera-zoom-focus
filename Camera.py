#!/bin/python3

from threading import Thread
import cv2 as cv

class Camera :

    def __init__(self, id=0, image_res=None):
        self.id = id
        self.cam = None
        
        try :
            self.cam = Libc_Camera(0, image_res)
        except :
            print("Libcamera not detected. Using default camera codec.")
            self.cam = Default_Camera(id, image_res)

    def start(self):
        self.cam.start()
    
    def stop(self):
        self.cam.stop()

    def read(self):
        return self.cam.read()




class Default_Camera :
    id=None

    def __init__(self, id=0, image_res=None) :
        self.id = id

        self.capture = cv.VideoCapture(id)
        if image_res != None :
            self.capture.set(cv.CAP_PROP_FRAME_WIDTH, image_res[0])
            self.capture.set(cv.CAP_PROP_FRAME_HEIGHT, image_res[1])

        ret,frame = self.capture.read()
        if not ret:
            print("Can't receive frame.")
            return

        self.frame = frame

        self.stopped = True

        self.t = Thread(target=self.update, args=())
        self.t.daemon = True

    def start(self):
        if not self.stopped :
            print("Camera already started.")
            return
        self.stopped = False
        self.t.start()

    def update(self):
        while True :
            if self.stopped :
                break
            ret,frame = self.capture.read()
            if not ret :
                print("Can't receive frame.")
                break
            self.frame = frame


    def read(self):
        return self.frame

    def stop(self):
        if self.stopped :
            print("Camera already stopped.")
            return
        self.stopped = True
        self.capture.release()

class Libc_Camera :

    def __init__(self, id=0, image_res=None):
        from picamera2 import Picamera2
        from libcamera import ColorSpace
        
        cam = self.cam = Picamera2(id)
        
        if image_res != None:
            config = cam.create_video_configuration(main={"size": image_res}, colour_space=ColorSpace.Sycc())
        else:
            config = cam.create_video_configuration(colour_space=ColorSpace.Sycc())

        cam.configure(config)
        cam.framrate = 30
        cam.start()
            
        frame = self.cam.capture_array("main")
        self.frame = cv.cvtColor(frame, cv.COLOR_RGB2BGR)

        self.stopped = True
        self.t = Thread(target=self.update, args=())
        self.t.daemon = True
         
    def start(self):
        if not self.stopped :
            print("Camera already started.")
            return
        self.stopped = False
        self.t.start()

    def update(self):
        while True :
            if self.stopped is True :
                break
            frame = self.cam.capture_array("main")
            self.frame = cv.cvtColor(frame, cv.COLOR_RGB2BGR)

    def read(self):
        return self.frame

    def stop(self):
        if self.stopped :
            print("Camera already stopped.")
            return
        self.stopped = True


def get_laplacian(mat):
    mat=cv.GaussianBlur(mat,(3,3), 0)
    mat=cv.cvtColor(mat, cv.COLOR_BGR2GRAY)
    mat=cv.Laplacian(mat, cv.CV_16U, ksize=3)
    mat=cv.convertScaleAbs(mat)
    return mat



def test():
    camera = Camera(0)
    camera.start()

    cv.namedWindow("Input", cv.WINDOW_KEEPRATIO)
    cv.namedWindow("Output", cv.WINDOW_NORMAL)

    while True:
        mat=camera.read()
        cv.imshow("Input", mat)
        mat=get_laplacian(mat)
        cv.imshow("Output", mat)

        if cv.waitKey(1) == ord('q'):
            break
    
    camera.stop()
    cv.destroyAllWindows()

if __name__ == "__main__":
    test()